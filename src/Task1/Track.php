<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private array $cars;

    public function __construct(
        private float $lapLength,
        private int $lapsNumber
    ) {
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $raceLength = $this->totalRaceLength();
        $carsRaceTime = [];
        foreach ($this->cars as $car) {
            $pitStopsTime = $this->pitStopsTime($raceLength, $car);
            $raceDriveTime = $this->raceDriveTime($raceLength, $car);
            $carsRaceTime[] = $pitStopsTime + $raceDriveTime;
        }
        $bestCarNumber = array_search(min($carsRaceTime), $carsRaceTime);
        return $this->cars[$bestCarNumber];
    }

    public function totalRaceLength(): float
    {
        return $this->getLapsNumber() * $this->getLapLength();
    }

    public function pitStopsTime(float $lapLength, Car $car): float|int
    {
        $fuelNeed = $lapLength / 100 * $car->getFuelConsumption();
        $pitStopsAmount = $fuelNeed / $car->getFuelTankVolume();
        return $pitStopsAmount * $car->getPitStopTime();
    }

    public function raceDriveTime(float $lapLength, Car $car): int|float
    {
        return $lapLength / $car->getSpeed() * 60 * 60;
    }
}
