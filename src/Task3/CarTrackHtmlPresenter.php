<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;
use phpDocumentor\Reflection\Element;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $cars = $track->all();
        $html = $this->createCarsInRaceElement($cars);
        $html .= $this->createCarsElement($cars);

        return $html;
    }

    public function createCarsInRaceElement(array $cars): string
    {
        $html = '<div>Cars in race: ';
        foreach ($cars as $car) {
            $html .= $car->getName(). ', ';
        }

        $html = rtrim($html, ', ');
        $html .= '</div>';

        return $html;
    }

    public function createCarElement(Car $car): string
    {
        $element = '<div style="padding: 10px"><p>';
        $element .= 'Id: ' .$car->getId(). '<br>';
        $element .= 'Car: ' .$car->getName(). '<br>';
        $element .= 'Speed: ' . $car->getSpeed(). '<br>';
        $element .= 'Pit stop time: ' . $car->getPitStopTime(). '<br>';
        $element .= 'Fuel consumption: ' . $car->getFuelConsumption(). '<br>';
        $element .= 'Fuel tank volume: ' . $car->getFuelTankVolume(). '<br>';
        $element .= $this->createCarElementImg($car);
        $element .= '</p></div>';

        return $element;
    }

    public function createCarElementImg(Car $car): string
    {
        return '<img src="'.$car->getImage().'">';
    }

    public function createCarsElement(array $cars):string
    {
        $html = '<div style="display: flex; flex-direction: row">';

        foreach ($cars as $car) {
            $html .= $this->createCarElement($car);
        }
        $html .= '</div>';

        return $html;
    }
}
